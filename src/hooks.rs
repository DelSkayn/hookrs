
use std::collections::HashMap;

use std::io::{self,Read};
use std::path::Path;
use std::{fs,error,fmt};
use serde_json::{self,Value,Number};

use ::rocket::{http,self};
use ::rocket::http::Status;
use ::rocket::response::status::Custom;
use ::rocket_contrib::Json;
use header::Headers;
use command::Command;

#[derive(Debug)]
pub struct Hooks(HashMap<String,Hook>);

#[derive(Debug)]
struct Hook{
    name: String,
    ensure: Vec<Ensure>,
    command: Command,
}

#[derive(PartialEq,Debug)]
enum Restrain{
    Num(Number),
    Str(String),
}


#[derive(Debug)]
enum RestrainType{
    Header,
    Body,
}

impl  RestrainType{
    fn from_str(s: &str) -> Option<Self>{
        match s {
            "body" => Some(RestrainType::Body),
            "header" => Some(RestrainType::Header),
            _ => None,
        }
    }
}

impl<'a> From<&'a str> for Restrain{
    fn from(e: &str) -> Restrain{
        Restrain::Str(e.to_string())
    }
}

impl Restrain{
    fn from_value(v: &Value) -> Option<Self>{
        match *v{
            Value::Number(ref x) => Some(Restrain::Num(x.clone())),
            Value::String(ref x) => Some(Restrain::Str(x.clone())),
            _ => None
        }
    }
}

#[derive(Debug)]
struct Ensure{
    key: String,
    ty: RestrainType,
    restrain: Restrain,
    reason: Option<String>,
}

#[derive(Debug)]
pub enum HookError{
    Io(io::Error),
    Parse(serde_json::Error),
    InvalidConfig(String),
}

impl From<io::Error> for HookError{
    fn from(e: io::Error) -> Self{
        HookError::Io(e)
    }
}

impl From<serde_json::Error> for HookError{
    fn from(e: serde_json::Error) -> Self{
        HookError::Parse(e)
    }
}

impl fmt::Display for HookError{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            HookError::Io(ref e) => write!(f,"Io error: {}",e),
            HookError::Parse(ref e) => write!(f,"Parse error: {}",e),
            HookError::InvalidConfig(ref e) => write!(f,"Configuration erro: {}",e),
        }
    }
}

impl error::Error for HookError{
    fn description(&self) -> &str{
        match *self{
            HookError::Io(ref e) => e.description(),
            HookError::Parse(ref e) => e.description(),
            HookError::InvalidConfig(_) => "Invalid configuration file",
        }
    }

    fn cause(&self) -> Option<&error::Error>{
        match *self{
            HookError::Io(ref e) => Some(e),
            HookError::Parse(ref e) => Some(e),
            HookError::InvalidConfig(_) => None,
        }
    }
}


pub fn read_config<P: AsRef<Path>>(path: P) -> Result<Hooks,HookError>{
    let file = fs::File::open(path)?;
    let value: Value = serde_json::from_reader(file)?;
    if !value.is_object(){
        return Err(HookError::InvalidConfig("Root value is not an object".to_string()));
    }
    let root = value.as_object().unwrap();
    let mut hooks = HashMap::new();

    for (k,v) in root{
        if !v.is_object(){
            return Err(HookError::InvalidConfig(format!("Hook \"{}\" is not an object",k)));
        }
        let hook = v.as_object().unwrap();
        if !hook.contains_key("ensure"){
            return Err(HookError::InvalidConfig(format!("hook \"{}\" does not have any requirements",k)));
        }

        if !hook.contains_key("command"){
            return Err(HookError::InvalidConfig(format!("hook \"{}\" does not have any commands to run",k)));
        }

        

        let mut res_hook = Hook{
            name: k.clone(),
            ensure: Vec::new(),
            command: serde_json::from_value(hook.get("command").unwrap().clone())?,
        };

        let ensure = hook.get("ensure").unwrap();
        if !ensure.is_object(){
            return Err(HookError::InvalidConfig(format!("The ensure value of hook \"{}\" is not an object",k)));
        }
        let ensure = ensure.as_object().unwrap();

        for (ensure_k,ensure_v) in ensure.iter(){
            if !ensure_v.is_object(){
                return Err(HookError::InvalidConfig(format!("Ensure \"{}\" in hook \"{}\" is not an object",ensure_k,k)));
            }
            let ensure_v = ensure_v.as_object().unwrap();
            if !ensure_v.contains_key("value"){
                return Err(HookError::InvalidConfig(format!("Ensure \"{}\" in hook \"{}\" does not contain a value",ensure_k,k)));
            }
            if !ensure_v.contains_key("type"){
                return Err(HookError::InvalidConfig(format!("Ensure \"{}\" in hook \"{}\" does not contain a type",ensure_k,k)));
            }
            if let Some(x) = ensure_v.get("reason"){
                if !x.is_string(){
                    return Err(HookError::InvalidConfig(format!("The value of reason in ensure \"{}\" in hook \"{}\" is not a string",ensure_k,k)));
                }
            }
            if let Some(x) = ensure_v.get("type"){
                if !x.is_string(){
                    return Err(HookError::InvalidConfig(format!("The value of type in ensure \"{}\" in hook \"{}\" is not a string",ensure_k,k)));
                }
            }
            let restrain = if let Some(x) = Restrain::from_value(ensure_v.get("value").unwrap()){
                x
            }else{
                    return Err(HookError::InvalidConfig(format!("Could not create a valid constrain in ensure \"{}\" in hook \"{}\" is not a string",ensure_k,k)));
            };
            let ty = if let Some(x) = RestrainType::from_str(ensure_v.get("type").unwrap().as_str().unwrap()){
                x
            }else{
                    return Err(HookError::InvalidConfig(format!("Not in valid type in ensure \"{}\" in hook \"{}\" is not a string",ensure_k,k)));
            };
            res_hook.ensure.push(Ensure{
                ty: ty,
                key: ensure_k.clone(),
                restrain: restrain,
                reason: ensure_v.get("reason").map(|e| e.as_str().unwrap().to_string()),
            })
        }

        hooks.insert(k.clone(),res_hook);
    }

    Ok(Hooks(hooks))
}


#[post("/hook/<name>", data="<data>")]
fn hooks(name: String,hooks: rocket::State<Hooks>,headers: Headers,data: Json<Value>) -> Result<String,Custom<String>>{
    let data = data.into_inner();
    let hook = if let Some(x) = hooks.0.get(&name){
        x
    }else{
        return Err(Custom(Status::BadRequest,"Not a valid hook".to_string()));
    };
    debug!("{:#?}",data);
    for e in hook.ensure.iter(){
        match e.ty{
            RestrainType::Header => {
                let header_value = if let Some(x) = headers.0.get_one(&e.key){
                    x
                }else{
                    let res = e.reason.as_ref().cloned().unwrap_or("Invalid header value".to_string());
                    return Err(Custom(Status::BadRequest,res));
                };
                if e.restrain != header_value.into(){
                    let res = e.reason.as_ref().cloned().unwrap_or("Invalid header value".to_string());
                    return Err(Custom(Status::BadRequest,res));
                }
            }
            RestrainType::Body => {
                if let Some(x) = lookup_key(&data,&e.key){
                    if let Some(x) = Restrain::from_value(x){
                        if x != e.restrain{
                            let res = e.reason.as_ref().cloned().unwrap_or("Invalid body value".to_string());
                            return Err(Custom(Status::BadRequest,res));
                        }
                    }else{
                        let res = e.reason.as_ref().cloned().unwrap_or("Invalid body value".to_string());
                        return Err(Custom(Status::BadRequest,res));
                    }
                }else{
                    let res = e.reason.as_ref().cloned().unwrap_or("body value not pressent".to_string());
                    return Err(Custom(Status::BadRequest,res));
                }
            }
        }
    }
    info!("Hook succesfull, running command");
    hook.command.execute();
    Ok("Hook succesfull.".to_string())
}

fn lookup_key<'a>(value: &'a Value,key: &str) -> Option<&'a Value>{
    if let Some(loc) = key.find("."){
        if let Some(map) = value.as_object(){
            let (k,r) = key.split_at(loc);
            map.get(k).and_then(|v|{
                lookup_key(v,&r[1..])
            })
        }else{
            None
        }
    }else{
        value.as_object().and_then(|e|{
            e.get(key)
        })
    }
}
