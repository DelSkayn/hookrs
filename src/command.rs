
use std::path::{Path,PathBuf};
use ::serde::Deserialize;
use ::serde_json::{Value,Error};

use std::{thread,process};

#[derive(Deserialize,Clone,Debug)]
pub struct Command{
    dir: PathBuf,
    execute: Vec<String>,
}

impl Command{
    pub fn execute(&self){
        let s = self.clone();
        thread::spawn(move ||{
            for command in s.execute.iter(){
                let cmd = process::Command::new("sh")
                    .current_dir(&s.dir)
                    .arg("-c")
                    .arg(command)
                    .status();

                match cmd{
                    Ok(x) => if !x.success(){
                        warn!("Hook failed, command \"{}\" was un succesfull",command);
                        return;
                    }
                    Err(e) => {
                        warn!("Error during execution of command: {}",e);
                    }
                }
            }
        });
    }
}
