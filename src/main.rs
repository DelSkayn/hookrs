
#![feature(plugin)]
#![plugin(rocket_codegen)]
#![allow(dead_code)]
#![allow(unused_imports)]

extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate log;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate clap;
extern crate nix;

use clap::{Arg, App, SubCommand,AppSettings};



mod logger;
mod hooks;
mod header;
mod command;




fn main(){
    logger::Logger::init().unwrap();
    let app = App::new("Hookrs")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Mees Delzenne")
        .about("Tool for executing code on arbetrary json request.")
        .global_setting(AppSettings::ColorAuto)
        .arg(Arg::with_name("Ip")
             .takes_value(true)
             .help("the ip on which the tool listens for requests")
             .required(true)
             .long("ip")
             .short("i")
             .value_name("ADDRESS"))
        .arg(Arg::with_name("Port")
             .takes_value(true)
             .help("The port on which the tool listens for requests")
             .required(true)
             .long("port")
             .short("p")
             .value_name("PORT"))
        .arg(Arg::with_name("Hook file location")
             .takes_value(true)
             .help("The location of the file which specifies the hooks for the tool, defualts to \"./hooks.json\"")
             .default_value("./hooks.json")
             .long("config")
             .short("c")
             .value_name("CONFIG"))
        .get_matches();

    println!("{:#?}",app);
    let ip = app.value_of("Ip").unwrap();
    let port = if let Ok(x) = app.value_of("Port").unwrap().parse::<u16>(){
        x
    }else{
        error!("Invalid port");
        return;
    };
    let env = if cfg!(debug_assertions) {
        rocket::config::Environment::Development
    }else{
        rocket::config::Environment::Production
    };

    let hooks_loc = app.value_of("Hook file location").unwrap();
    info!("Opening hook file at \"{}\"",hooks_loc);

    let hooks = match hooks::read_config(hooks_loc){
        Ok(x) => x,
        Err(e) => {
            error!("{}",e);
            return;
        }
    };

    let config = match rocket::Config::build(env)
        .address(ip)
        .port(port)
        .workers(2)
        .finalize()
    {
        Ok(x) => x,
        Err(e) => {
            error!("Invalid address: {}",e);
            return;
        }
    };


    rocket::custom(config,true)
        .manage(hooks)
        .mount("/", routes![hooks::hooks])
        .launch();
}

