
use rocket::http::HeaderMap;
use rocket::request::{FromRequest,Request,Outcome as ROutcome};
use rocket::outcome::Outcome;

pub struct Headers<'a>(pub HeaderMap<'a>);

impl<'a,'r> FromRequest<'a,'r> for Headers<'r>{
    type Error = ();
    fn from_request(request: &'a Request<'r>) -> ROutcome<Self, Self::Error>{
        Outcome::Success(Headers(request.headers().clone()))
    }
}
